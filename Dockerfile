# Stage 1
FROM golang:1.21-alpine3.18 as builder

# Copy code into the container. Note: copy to a dir instead of `.`, since $GOPATH may not contain a go.mod file
WORKDIR /build

COPY . .

# Build the Go Files
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o server .

# Stage 3
FROM alpine:3.18

# Add timezones
RUN apk add --no-cache tzdata

# Copy the bin from builder to root.
COPY --from=builder /build/server /

ENTRYPOINT ["./server", "interpreter"]

EXPOSE 80
