---
title : "Fictief register adressen en gebouwen interpreter"
description: "Documentation for the address and building records interpreter"
lead: ""
date: 2023-08-28T14:52:40+02:00
draft: true
toc: true
---

## Running locally
Clone [this repo](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-interpreter).

To run the development server:

Default settings are:
- Nats host: localhost:4222
- Backend URL of records db: http://localhost:8080

Run:
```shell
go run . interpreter
```

To overwrite the default settings

Run:
```shell
go run . interpreter --sdg-nats-host-address=127.0.0.1 --sdg-nats-port=4222 --sdg-backend-url=http://127.0.0.1:9000
```

Settings can also be set through env variables
```shell
SDG_NATS_SERVICE_HOST=127.0.0.1 SDG_NATS_SERVICE_PORT=4223 SDG_BACKEND_URL=http://localhost:9000 go run . interpreter
```
