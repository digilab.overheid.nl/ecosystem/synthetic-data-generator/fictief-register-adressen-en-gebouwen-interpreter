package model

import (
	"time"

	"github.com/google/uuid"
)

type Municipality struct {
	Name string `json:"name"`
	Code string `json:"code"`
}

type BuildingRegisteredEvent struct {
	ConstructedAt time.Time    `json:"constructedAt"`
	Surface       int32        `json:"surface"`
	Municipality  Municipality `json:"municipality"`
}

type BuildingEvent struct {
	ID            uuid.UUID `json:"id"`
	ConstructedAt time.Time `json:"constructedAt"`
	Surface       int32     `json:"surface"`
}

type AttachBuildingsToAddressEvent struct {
	Address      uuid.UUID    `json:"address"`
	Municipality Municipality `json:"municipality"`
}

type AttachBuildingsToAddress struct {
	Buildings []uuid.UUID `json:"buildings"`
	Addresses []uuid.UUID `json:"addresses"`
}
