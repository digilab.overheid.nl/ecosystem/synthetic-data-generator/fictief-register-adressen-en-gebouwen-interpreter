package model

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type Event struct {
	ID           uuid.UUID       `json:"id"`
	RegisteredAt time.Time       `json:"registeredAt"`
	OccurredAt   time.Time       `json:"occurredAt"`
	SubjectIDs   []uuid.UUID     `json:"subjectIds"`
	EventType    string          `json:"eventType"`
	EventData    json.RawMessage `json:"eventData"`
}
