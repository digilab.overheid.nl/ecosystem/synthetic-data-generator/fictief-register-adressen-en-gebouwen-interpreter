package cmd

import (
	"fmt"
	"log"
	"net"

	"github.com/nats-io/nats.go"
	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-interpreter/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-interpreter/process"
)

var interpreterOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	Nats struct {
		Host string
		Port string
	}
	BackendURL   string
	Municipality string
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	interpreterCommand.Flags().StringVarP(&interpreterOpts.Nats.Host, "sdg-nats-service-host", "", "127.0.0.1", "Address of NATS")
	interpreterCommand.Flags().StringVarP(&interpreterOpts.Nats.Port, "sdg-nats-service-port", "", "4222", "Port of NATS")

	interpreterCommand.Flags().StringVarP(&interpreterOpts.BackendURL,
		"sdg-backend-url", "", "http://0.0.0.0:8080", "Base URL of the Fictieve Register")

	interpreterCommand.Flags().StringVarP(&interpreterOpts.Municipality,
		"sdg-municipality-code", "", "", "Municipality Code that this interpreter is working for")
}

var interpreterCommand = &cobra.Command{ //nolint:exhaustruct,gochecknoglobals // this is the recommended way to use cobra
	Use:   "interpreter",
	Short: "Run the event interpreter",
	Run: func(cmd *cobra.Command, args []string) {
		proc := process.NewProcess()

		fmt.Printf("interpreterOpts: %v\n", interpreterOpts)

		conn, err := nats.Connect(fmt.Sprintf("nats://%s", net.JoinHostPort(interpreterOpts.Nats.Host, interpreterOpts.Nats.Port)))
		if err != nil {
			log.Fatal(fmt.Errorf("nats connect failed: %w", err))
		}

		fmt.Println("Starting")

		stream, err := conn.JetStream()
		if err != nil {
			log.Fatal(fmt.Errorf("jet stream failed: %w", err))
		}

		app := application.NewApplication(interpreterOpts.Municipality, interpreterOpts.BackendURL)

		sub, err := stream.Subscribe("events.frag.>", app.HandleEvents)
		if err != nil {
			log.Fatal(fmt.Errorf("subscribe failed: %w", err))
		}

		proc.Wait()

		if err := sub.Unsubscribe(); err != nil {
			log.Fatal(fmt.Errorf("unsubscribe failed: %w", err))
		}

		if err := sub.Drain(); err != nil {
			log.Fatal(fmt.Errorf("subscription drain failed: %w", err))
		}

		if err := conn.Drain(); err != nil {
			log.Fatal(fmt.Errorf("nats drain failed: %w", err))
		}

		conn.Close()
	},
}
